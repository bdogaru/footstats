import React, { Component } from 'react';
import Team from './components/Team';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      homeTeam: 'Home',
      awayTeam: 'Away'
    }

    this.checkState = this.checkState.bind(this);
  }

  checkState() {
    console.log(this.state);
  }

  render() {
    return (
      <div className="App">
        <button onClick={ () => { this.setState({ awayTeam: 'qwewqe' }) } }>Change</button>
        <Team teamName={ this.state.awayTeam } onClick={ this.checkState }/>
      </div>
    );
  }
}

export default App;
